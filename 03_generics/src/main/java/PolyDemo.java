import ru.antara.model.Animal;
import ru.antara.model.Cat;
import ru.antara.model.HomeCat;
import ru.antara.model.WildCat;

public class PolyDemo {
    public static void main(String[] args) {

        Animal it = new HomeCat("Барсик");
        System.out.println("is HomeCat instance of Cat? " + (it instanceof Cat));

        System.out.println(it.toString());

        Cat tom = new WildCat("Кыс-кыс");
        System.out.println("is WildCat instance of HomeCat? " + (tom instanceof HomeCat));
        System.out.println(tom.toString());

        HomeCat fluffy = new HomeCat("Толстяк");
        fluffy.trySuper();
    }

}
