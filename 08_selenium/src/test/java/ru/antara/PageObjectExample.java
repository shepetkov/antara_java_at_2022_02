package ru.antara;

import java.time.Duration;
import java.util.stream.Stream;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

import ru.antara.pages.ResultPage;
import ru.antara.pages.SearchPage;

@DisplayName("тест с page object")
class PageObjectExample {

    private static WebDriver driver;

    private final SearchPage searchPage = new SearchPage();
    private final ResultPage resultPage = new ResultPage();

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("унесенные", "Япония", "Унесённые призраками", 5, "Унесенные волками (сериал)", new String[]{"аниме", "драма"})
        );
    }

    @BeforeAll
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\projects\\school\\antara_java_at_2021_09\\chromedriver.exe");
        driver = new ChromeDriver();
        Configuration.browser = "chrome";
        Configuration.holdBrowserOpen = false;
        Configuration.baseUrl = "https://www.kinopoisk.ru/s/";
    }

    @AfterAll
    public static void teardown() {
        driver.quit();
    }

    @ParameterizedTest
    @MethodSource("testData")
    void searchResultTest(String searchStr, String country, String topResult, int size, String secondResult, String ...genres) {
        searchPage.open().inputSearch(searchStr).setGenre(genres).seCountry(country).search();
        resultPage.shouldHaveAtTop(topResult).shouldHaveSizeAndText(size, secondResult);
    }

}
